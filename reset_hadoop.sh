#!/bin/bash
hadoop namenode -format
rm -rf /tmp/hbase*
$HADOOP_HOME/sbin/start-dfs.sh
$HADOOP_HOME/sbin/start-yarn.sh
$HBASE_HOME/bin/hbase-daemon.sh --config $HBASE_HOME/conf start master 
jps
